## Objective

This template is a dummy deploy job used for demo. It just echo "Deploying". 

## How to use it

1. Copy/paste the quick use above in your `.gitlab-ci.yml` file
1. Well done, your job is ready to work ! 😀

## Variables

| Name | Description | Default |
| ---- | ----------- | ------- |
| `MESSAGE` | Message to echo | `Deploy 🚀 !` |
| `IMAGE_TAG` | Image to scan | `3.18` |
